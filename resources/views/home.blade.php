<!DOCTYPE html>
<html lang="ja">
<head>
    <title>{{ config('app.name') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <a href="https://aobun-api.herokuapp.com/api/works">https://aobun-api.herokuapp.com/api/works</a><br /><br />
    <a href="https://aobun-api.herokuapp.com/api/works?title=%E7%8C%AB">https://aobun-api.herokuapp.com/api/works?title=%E7%8C%AB</a><br /><br />
    <a href="https://aobun-api.herokuapp.com/api/works/2">https://aobun-api.herokuapp.com/api/works/2</a><br /><br />
    <br />
    <a href="https://aobun-api.herokuapp.com/api/people">https://aobun-api.herokuapp.com/api/people</a><br /><br />
    <a href="https://aobun-api.herokuapp.com/api/people?name=%E5%A4%8F">https://aobun-api.herokuapp.com/api/people?name=%E5%A4%8F</a><br /><br />
    <a href="https://aobun-api.herokuapp.com/api/people/5">https://aobun-api.herokuapp.com/api/people/5</a><br /><br />

    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
