<?php

namespace Database\Seeders;

use App\Models\Person;
use App\Models\Work;
use Illuminate\Database\Seeder;
use League\Csv\Reader;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $csv = Reader::createFromPath(storage_path('app/list_csv/list_person_all_utf8.csv'));
        $csv->setHeaderOffset(0);
        foreach ($csv->getRecords() AS $record) {
            Person::updateOrCreate(['id' => $record['人物ID']], [
                'full_name' => $record['著者名']
            ]);

            Work::updateOrCreate(['id' => $record['作品ID']], [
                'person_id' => $record['人物ID'],
                'title' => $record['作品名'],
                'kana_type' => $record['仮名遣い種別'],
                'translator' => $record['翻訳者名等'],
                'inputter' => $record['入力者名'],
                'corrector' => $record['校正者名'],
                'status' => $record['状態'],
                'status_updated_at' => $record['状態の開始日'],
                'source_title1' => $record['底本名'],
                'source_publisher1' => $record['出版社名'],
                'input_edition1' => $record['入力に使用した版'],
                'correct_edition1' => $record['校正に使用した版'],
            ]);
        }

        $csv = Reader::createFromPath(storage_path('app/list_csv/list_person_all_extended_utf8.csv'));
        $csv->setHeaderOffset(0);
        foreach ($csv->getRecords() AS $record) {
            Person::updateOrCreate(['id' => $record['人物ID']], [
                'last_name' => $record['姓'],
                'first_name' => $record['名'],
                'last_name_kana' => $record['姓読み'],
                'first_name_kana' => $record['名読み'],
                'last_name_kana_sort' => $record['姓読みソート用'],
                'first_name_kana_sort' => $record['名読みソート用'],
                'last_name_roman' => $record['姓ローマ字'],
                'first_name_roman' => $record['名ローマ字'],
                'role' => $record['役割フラグ'],
                'birthday' => $record['生年月日'],
                'death_date' => $record['没年月日'],
            ]);

            Work::updateOrCreate(['id' => $record['作品ID']], [
                'title' => $record['作品名'],
                'title_kana' => $record['作品名読み'],
                'title_kana_sort' => $record['ソート用読み'],
                'subtitle' => $record['副題'],
                'subtitle_kana' => $record['副題読み'],
                'original_title' => $record['原題'],
                'person_id' => $record['人物ID'],
                'first_coming' => $record['初出'],
                'classification' => $record['分類番号'],
                'kana_type' => $record['文字遣い種別'],
                'source_title1' => $record['底本名1'],
                'source_publisher1' => $record['底本出版社名1'],
                'source_published_at1' => $record['底本初版発行年1'],
                'input_edition1' => $record['入力に使用した版1'],
                'correct_edition1' => $record['校正に使用した版1'],
                'parent_source_book1' => $record['底本の親本名1'],
                'parent_source_publisher1' => $record['底本の親本出版社名1'],
                'parent_source_published_at1' => $record['底本の親本初版発行年1'],
                'source_title2' => $record['底本名2'],
                'source_publisher2' => $record['底本出版社名2'],
                'source_published_at2' => $record['底本初版発行年2'],
                'input_edition2' => $record['入力に使用した版2'],
                'correct_edition2' => $record['校正に使用した版2'],
                'parent_source_book2' => $record['底本の親本名2'],
                'parent_source_publisher2' => $record['底本の親本出版社名2'],
                'parent_source_published_at2' => $record['底本の親本初版発行年2'],
                'copyright' => $record['作品著作権フラグ'] == 'なし' ? 'disable' : 'enable',
                'released_at' => $record['公開日'],
                'aozora_updated_at' => $record['最終更新日'],
                'card_url' => $record['図書カードURL'],
                'inputter' => $record['入力者'],
                'corrector' => $record['校正者'],
                'text_file_url' => $record['テキストファイルURL'],
                'text_file_updated_at' => $record['テキストファイル最終更新日'],
                'text_file_encode' => $record['テキストファイル符号化方式'],
                'text_file_charset' => $record['テキストファイル文字集合'],
                'text_file_correct_count' => $record['テキストファイル修正回数'],
                'html_file_url' => $record['XHTML/HTMLファイルURL'],
                'html_file_updated_at' => $record['XHTML/HTMLファイル最終更新日'],
                'html_file_encode' => $record['XHTML/HTMLファイル符号化方式'],
                'html_file_charset' => $record['XHTML/HTMLファイル文字集合'],
                'html_file_correct_count' => $record['XHTML/HTMLファイル修正回数'],
            ]);
        }
    }
}
