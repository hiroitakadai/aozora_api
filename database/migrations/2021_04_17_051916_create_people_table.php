<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->comment('著者名')->index();
            $table->string('last_name')->comment('姓')->nullable();
            $table->string('first_name')->comment('名')->nullable();
            $table->string('last_name_kana')->comment('姓読み')->nullable();
            $table->string('first_name_kana')->comment('名読み')->nullable();
            $table->string('last_name_kana_sort')->comment('姓読みソート用')->nullable();
            $table->string('first_name_kana_sort')->comment('名読みソート用')->nullable();
            $table->string('last_name_roman')->comment('姓ローマ字')->nullable();
            $table->string('first_name_roman')->comment('名ローマ字')->nullable();
            $table->string('role')->comment('役割')->nullable();
            $table->string('birthday')->comment('生年月日')->nullable()->index();
            $table->string('death_date')->comment('没年月日')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
