<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('作品名')->nullable()->index();
            $table->string('title_kana')->comment('作品名読み')->nullable();
            $table->string('title_kana_sort')->comment('ソート用読み')->nullable();
            $table->string('subtitle')->comment('副題')->nullable()->index();
            $table->string('subtitle_kana')->comment('副題読み')->nullable();
            $table->string('original_title')->comment('原題')->nullable();
            $table->unsignedBigInteger('person_id')->comment('人物ID')->index();
            $table->string('translator')->comment('翻訳者名等')->nullable()->index();
            $table->text('first_coming')->comment('初出')->nullable();
            $table->string('classification')->comment('分類番号')->nullable()->index();
            $table->string('kana_type')->comment('仮名遣い種別')->nullable();
            $table->string('source_title1')->comment('底本名1')->nullable();
            $table->string('source_publisher1')->comment('底本出版社名1')->nullable();
            $table->string('source_published_at1')->comment('底本初版発行年1')->nullable();
            $table->string('input_edition1')->comment('入力に使用した版1')->nullable();
            $table->string('correct_edition1')->comment('校正に使用した版1')->nullable();
            $table->string('parent_source_book1')->comment('底本の親本名1')->nullable();
            $table->string('parent_source_publisher1')->comment('底本の親本出版社名1')->nullable();
            $table->string('parent_source_published_at1')->comment('底本の親本初版発行年1')->nullable();
            $table->string('source_title2')->comment('底本名2')->nullable();
            $table->string('source_publisher2')->comment('底本出版社名2')->nullable();
            $table->string('source_published_at2')->comment('底本初版発行年2')->nullable();
            $table->string('input_edition2')->comment('入力に使用した版2')->nullable();
            $table->string('correct_edition2')->comment('校正に使用した版2')->nullable();
            $table->string('parent_source_book2')->comment('底本の親本名2')->nullable();
            $table->string('parent_source_publisher2')->comment('底本の親本出版社名2')->nullable();
            $table->string('parent_source_published_at2')->comment('底本の親本初版発行年2')->nullable();
            $table->string('inputter')->comment('入力者')->nullable();
            $table->string('corrector')->comment('校正者')->nullable();
            $table->enum('copyright', ['enable', 'disable'])->comment('作品著作権フラグ')->default('enable');
            $table->string('released_at')->comment('公開日')->nullable();
            $table->string('aozora_updated_at')->comment('最終更新日')->nullable();
            $table->string('card_url')->comment('図書カードURL')->nullable();
            $table->string('text_file_url')->comment('テキストファイルURL')->nullable();
            $table->string('text_file_updated_at')->comment('テキストファイル最終更新日')->nullable();
            $table->string('text_file_encode')->comment('テキストファイル符号化方式')->nullable();
            $table->string('text_file_charset')->comment('テキストファイル文字集合')->nullable();
            $table->string('text_file_correct_count')->comment('テキストファイル修正回数')->nullable();
            $table->string('html_file_url')->comment('HTMLファイルURL')->nullable();
            $table->string('html_file_updated_at')->comment('HTMLファイル最終更新日')->nullable();
            $table->string('html_file_encode')->comment('HTMLファイル符号化方式')->nullable();
            $table->string('html_file_charset')->comment('HTMLファイル文字集合')->nullable();
            $table->string('html_file_correct_count')->comment('HTMLファイル修正回数')->nullable();
            $table->enum('status', ['公開'])->comment('状態')->nullable();
            $table->string('status_updated_at')->comment('状態の開始日')->nullable();
            $table->timestamps();

            $table->foreign('person_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
