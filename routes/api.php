<?php

use App\Http\Resources\PersonResource;
use App\Http\Resources\WorkResource;
use App\Models\Person;
use App\Models\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('works', function (Request $request){
    return WorkResource::collection(Work::where('title', 'like', '%' . $request->title. '%')->paginate());
})->name('works.index');
Route::get('works/{work}', function (Work $work){
    return new WorkResource($work);
})->name('works.show');

Route::get('people', function (Request $request){
    return PersonResource::collection(Person::where('full_name', 'like', '%' . $request->name. '%')->paginate());
})->name('people.index');
Route::get('people/{person}', function (Person $person){
    return new PersonResource($person);
})->name('people.show');
