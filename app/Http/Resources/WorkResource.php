<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'person' => new PersonResource($this->person),
            'translator' => $this->translator,
            'classification' => $this->classification,
            'copyright' => $this->copyright,
        ];

        if ($request->routeIs('api.works.show')){
            $data['text'] = $this->text();
        }

        return $data;
    }
}
