<?php

namespace App\Models;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class Work extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     *
     */
    protected static function booted()
    {
        static::addGlobalScope('copyright', function (Builder $builder) {
            $builder->where('copyright', 'disable');
        });
    }

    /**
     * @return BelongsTo
     */
    public function person(): BelongsTo
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * @return string
     * @throws FileNotFoundException
     */
    public function text(): String
    {
        $url = explode('/', parse_url($this->text_file_url, PHP_URL_PATH));
        $path = implode('/', [
            'aozorabunko_text',
            $url[1],
            $url[2],
            $url[3],
            str_replace('.zip', '', $url[4]),
            str_replace('.zip', '.txt', $url[4])
        ]);
        return mb_convert_encoding(Storage::get($path), 'UTF-8', ['SJIS', 'UTF-8']);
    }
}
